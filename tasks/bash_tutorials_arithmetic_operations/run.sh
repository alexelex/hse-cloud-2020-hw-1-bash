#!/bin/bash

read -r input

result=$(echo "scale=10; $input" | bc)

printf "%.3f" "$result"

