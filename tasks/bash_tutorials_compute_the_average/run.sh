#!/bin/bash

read -r n
sum=0

for i in $(seq 1 $n); do
    read -r value
    sum=$(( $sum + $value ))
done

echo "scale=3; $sum / $n" | bc

