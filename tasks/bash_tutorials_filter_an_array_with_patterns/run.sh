#!/bin/bash

readarray -t countries

for country in "${countries[@]}"
do
    if [[ ! $country =~ "a" ]]; then
        if [[ ! $country =~ "A" ]]; then
            echo "$country"
        fi
    fi
done

