#!/bin/bash

read -r cond

if [ "$cond" = "y" ]; then
    echo -n "YES"
elif [ "$cond" = "Y" ]; then
    echo -n "YES"
elif [ "$cond" = "n" ]; then
    echo -n "NO"
elif [ "$cond" = "N" ]; then
    echo -n "NO"
fi

