#!/bin/bash

read a
read b
read c

if [ $a = $b ]; then
    if [ $a = $c ]; then
        echo -n "EQUILATERAL"
    else
        echo -n "ISOSCELES"
    fi
elif [ $a = $c ]; then
    echo -n "ISOSCELES"
elif [ $b = $c ]; then
    echo -n "ISOSCELES"
else
    echo -n "SCALENE"
fi

