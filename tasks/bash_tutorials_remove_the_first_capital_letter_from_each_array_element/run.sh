#!/bin/bash

readarray -t countries

count=0

for i in ${countries[@]}
do

    part=$(echo ${i:1:${#i}})

    countries[$count]=".$part"
    count=$((count + 1))
done

echo ${countries[@]}

